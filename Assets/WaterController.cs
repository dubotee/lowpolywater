﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class WaterController : MonoBehaviour
{
    [DllImport("LowPolyWaterUtils", EntryPoint = "CreateVertexBuffer")]
    public static extern void CreateVertexBuffer(int size, float x, float y, float z);

    [DllImport("LowPolyWaterUtils", EntryPoint = "GetPairCheck")]
    public static extern int GetPairCheck(int index);

    [DllImport("LowPolyWaterUtils", EntryPoint = "GetPairCheckCacheX")]
    public static extern float GetPairCheckCacheX(int index);

    [DllImport("LowPolyWaterUtils", EntryPoint = "GetPairCheckCacheY")]
    public static extern float GetPairCheckCacheY(int index);

    [DllImport("LowPolyWaterUtils", EntryPoint = "GetPairCheckCacheZ")]
    public static extern float GetPairCheckCacheZ(int index);

    public int m_waterSize = 64;

    Mesh waveMesh;
    List<Vector3> newVertices = new List<Vector3>();
    List<int> newTriangles = new List<int>();
    List<Vector2> newUV = new List<Vector2>();

    //	public float scale = 0.5f;
    //	public float speed = 1.0f;
    public float noiseStrength = 2f;
    public float noiseWalk = 16f;

    public int vertexPartitioning = 8;

    [Header("Wave Settings")]
    public float m_waveStepLength_1 = 3;
    public float m_stepSpeed_1 = 2;
    public float m_stepHeight_1 = 3;
    public float m_stepSteepness_1 = 2;
    public Vector2 m_stepDirection_1 = Vector2.one;

    public float m_waveStepLength_2 = 3;
    public float m_stepSpeed_2 = 2;
    public float m_stepHeight_2 = 3;
    public float m_stepSteepness_2 = 2;
    public Vector2 m_stepDirection_2 = Vector2.one;

    public float m_waveStepLength_3 = 3;
    public float m_stepSpeed_3 = 2;
    public float m_stepHeight_3 = 3;
    public float m_stepSteepness_3 = 2;
    public Vector2 m_stepDirection_3 = Vector2.one;

    private Vector3[] baseHeight;
    private Vector3[] buffer_vertices;
    private Vector3[] backbuffer_vertices;
    private float[] buffer_vertices_length;
    private Vector3[] buffer_vertices_dir;
    private int currentPartition = 0;

    void Start()
    {
        Mesh mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        float x = transform.position.x;
        float y = transform.position.y;
        float z = transform.position.z;

        CreateVertexBuffer(m_waterSize, x, y ,z);

        for (int i = 0; i < m_waterSize; i++)
        {
            for (int j = 0; j < m_waterSize; j++)
            {
                newVertices.Add(new Vector3(x + i,     y, z + j     ));
                newVertices.Add(new Vector3(x + i + 1, y, z + j     ));
                newVertices.Add(new Vector3(x + i,     y, z + j - 1 ));
                newVertices.Add(new Vector3(x + i + 1, y, z + j     ));
                newVertices.Add(new Vector3(x + i,     y, z + j - 1 ));
                newVertices.Add(new Vector3(x + i + 1, y, z + j - 1 ));

                newTriangles.Add(i * (m_waterSize * 6) + (j * 6))    ;
                newTriangles.Add(i * (m_waterSize * 6) + (j * 6) + 1);
                newTriangles.Add(i * (m_waterSize * 6) + (j * 6) + 2);
                newTriangles.Add(i * (m_waterSize * 6) + (j * 6) + 3);
                newTriangles.Add(i * (m_waterSize * 6) + (j * 6) + 5);
                newTriangles.Add(i * (m_waterSize * 6) + (j * 6) + 4);

                newUV.Add(new Vector2(((float)i) / m_waterSize, ((float)j) / m_waterSize));
                newUV.Add(new Vector2(((float)i + 1) / m_waterSize, ((float)j) / m_waterSize));
                newUV.Add(new Vector2(((float)i) / m_waterSize, ((float)j - 1) / m_waterSize));
                newUV.Add(new Vector2(((float)i + 1) / m_waterSize, ((float)j) / m_waterSize));
                newUV.Add(new Vector2(((float)i) / m_waterSize, ((float)j - 1) / m_waterSize));
                newUV.Add(new Vector2(((float)i + 1) / m_waterSize, ((float)j - 1) / m_waterSize));
            }
        }

        mesh.Clear();
        mesh.vertices = newVertices.ToArray();
        mesh.triangles = newTriangles.ToArray();
        mesh.uv = newUV.ToArray();
        mesh.Optimize();
        mesh.RecalculateNormals();
        //TangentSolver.Solve(mesh);
    }

    void Update()
    {
        Mesh mesh = GetComponent<MeshFilter>().mesh;

        if (baseHeight == null)
            baseHeight = mesh.vertices;

        if (buffer_vertices == null)
            buffer_vertices = mesh.vertices;

        if (backbuffer_vertices == null)
            backbuffer_vertices = mesh.vertices;

        if (buffer_vertices_length == null)
            buffer_vertices_length = new float[baseHeight.Length];

        if (buffer_vertices_dir == null)
            buffer_vertices_dir = new Vector3[baseHeight.Length];

        Vector3[] vertices = new Vector3[baseHeight.Length];

        for (int i = 0; i < vertices.Length; i++)
        {
            if (i >= vertices.Length / vertexPartitioning * currentPartition && i < vertices.Length / vertexPartitioning * (currentPartition + 1))
            {
                Vector3 vertex = baseHeight[i];
                if (GetPairCheck(i) != -1)
                {
                    //buffer_vertices_length[i] = buffer_vertices_length[vertexPair[i]];
                    //buffer_vertices[i] = buffer_vertices[vertexPair[i]];
                }
                else
                {
                    /////////// old formation //////
                    //			vertex.y += Mathf.Sin(Time.time * speed+ baseHeight[i].x + baseHeight[i].y + baseHeight[i].z) * scale;
                    //			vertex.y += Mathf.PerlinNoise(baseHeight[i].x + noiseWalk, baseHeight[i].y + Mathf.Sin(Time.time * 0.1f)    ) * noiseStrength;
                    //			vertices[i] = vertex;
                    ///////////////////////////////

                    float w_1 = 2 * Mathf.PI / m_waveStepLength_1;
                    float phi_1 = m_stepSpeed_1 * w_1;

                    float w_2 = 2 * Mathf.PI / m_waveStepLength_2;
                    float phi_2 = m_stepSpeed_2 * w_2;

                    float w_3 = 2 * Mathf.PI / m_waveStepLength_3;
                    float phi_3 = m_stepSpeed_3 * w_3;

                    //			vertex.y = 2 * m_stepHeight * Mathf.Pow( (Mathf.Sin( Vector2.Dot(m_stepDirection, new Vector2(vertex.x, vertex.z)) * w + Time.time * phi ) + 1) / 2, m_stepPower );

                    //			Vector3 binormal = new Vector3(1, 0, vertex.y);
                    //			Vector3 tangent = new Vector3(0, 1, vertex.y);
                    //			Vector3 normal = Vector3.Cross(binormal, tangent);

                    float offset_x_1 = m_stepSteepness_1 * m_stepHeight_1 * m_stepDirection_1.x * Mathf.Cos(w_1 * Vector2.Dot(m_stepDirection_1, new Vector2(vertex.x, vertex.z)) + (phi_1 * Time.time));
                    float offset_y_1 = m_stepHeight_1 * Mathf.Sin(w_1 * Vector2.Dot(m_stepDirection_1, new Vector2(vertex.x, vertex.z)) * w_1 + Time.time * phi_1);
                    float offset_z_1 = m_stepSteepness_1 * m_stepHeight_1 * m_stepDirection_1.y * Mathf.Cos(w_1 * Vector2.Dot(m_stepDirection_1, new Vector2(vertex.x, vertex.z)) + (phi_1 * Time.time));

                    float offset_x_2 = m_stepSteepness_2 * m_stepHeight_2 * m_stepDirection_2.x * Mathf.Cos(w_2 * Vector2.Dot(m_stepDirection_2, new Vector2(vertex.x, vertex.z)) + (phi_2 * Time.time));
                    float offset_y_2 = m_stepHeight_2 * Mathf.Sin(w_2 * Vector2.Dot(m_stepDirection_2, new Vector2(vertex.x, vertex.z)) * w_2 + Time.time * phi_2);
                    float offset_z_2 = m_stepSteepness_2 * m_stepHeight_2 * m_stepDirection_2.y * Mathf.Cos(w_2 * Vector2.Dot(m_stepDirection_2, new Vector2(vertex.x, vertex.z)) + (phi_2 * Time.time));

                    float offset_x_3 = m_stepSteepness_3 * m_stepHeight_3 * m_stepDirection_3.x * Mathf.Cos(w_3 * Vector2.Dot(m_stepDirection_3, new Vector2(vertex.x, vertex.z)) + (phi_3 * Time.time));
                    float offset_y_3 = m_stepHeight_3 * Mathf.Sin(w_3 * Vector2.Dot(m_stepDirection_3, new Vector2(vertex.x, vertex.z)) * w_3 + Time.time * phi_3);
                    float offset_z_3 = m_stepSteepness_3 * m_stepHeight_3 * m_stepDirection_3.y * Mathf.Cos(w_3 * Vector2.Dot(m_stepDirection_3, new Vector2(vertex.x, vertex.z)) + (phi_3 * Time.time));

                    //Gerstner Waves
                    vertex = new Vector3(
                        vertex.x + offset_x_1 + offset_x_2 + offset_x_3,
                        offset_y_1 + offset_y_2 + offset_y_3,
                        vertex.z + offset_z_1 + offset_z_2 + offset_z_3
                    );

                    float randomNoise = Mathf.PerlinNoise(vertex.x + noiseWalk * Mathf.Cos(Time.time), vertex.z) * noiseStrength;
                    vertex.y *= randomNoise;

                    buffer_vertices_length[i] = Vector3.Magnitude(backbuffer_vertices[i] - vertex) / vertexPartitioning;
                    buffer_vertices_dir[i] = Vector3.Normalize(vertex - backbuffer_vertices[i]);
                    buffer_vertices[i] = vertex;
                }
            }
            else
            {
                //vertices[i] = buffer_vertices[i];
            }

            if (GetPairCheck(i) != -1)
            {
                backbuffer_vertices[i] = backbuffer_vertices[GetPairCheck(i)];

            }
            else
            {
                backbuffer_vertices[i] += buffer_vertices_dir[i] * buffer_vertices_length[i];
            }
            
        }

        mesh.vertices = backbuffer_vertices;
        mesh.RecalculateNormals();
        currentPartition = (currentPartition + 1) % vertexPartitioning;
    }
}

/*class TangentSolver 
{ 
    public static void Solve(Mesh mesh) 
    { 
        int triangleCount = mesh.triangles.Length / 3; int vertexCount = mesh.vertices.Length;
		
        Vector3[] tan1 = new Vector3[vertexCount];
        Vector3[] tan2 = new Vector3[vertexCount];
		
        Vector4[] tangents = new Vector4[vertexCount];
		
        for(long a = 0; a < triangleCount; a+=3)
        {
            long i1 = mesh.triangles[a+0];
            long i2 = mesh.triangles[a+1];
            long i3 = mesh.triangles[a+2];
			
            Vector3 v1 = mesh.vertices[i1];
            Vector3 v2 = mesh.vertices[i2];
            Vector3 v3 = mesh.vertices[i3];
			
            Vector2 w1 = mesh.uv[i1];
            Vector2 w2 = mesh.uv[i2];
            Vector2 w3 = mesh.uv[i3];
			
            float x1 = v2.x - v1.x;
            float x2 = v3.x - v1.x;
            float y1 = v2.y - v1.y;
            float y2 = v3.y - v1.y;
            float z1 = v2.z - v1.z;
            float z2 = v3.z - v1.z;
			
            float s1 = w2.x - w1.x;
            float s2 = w3.x - w1.x;
            float t1 = w2.y - w1.y;
            float t2 = w3.y - w1.y;
			
            float r = 1.0f / (s1 * t2 - s2 * t1);
			
            Vector3 sdir = new Vector3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
            Vector3 tdir = new Vector3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);
			
            tan1[i1] += sdir;
            tan1[i2] += sdir;
            tan1[i3] += sdir;
			
            tan2[i1] += tdir;
            tan2[i2] += tdir;
            tan2[i3] += tdir;
        }
		
		
        for (long a = 0; a < vertexCount; ++a)
        {
            Vector3 n = mesh.normals[a];
            Vector3 t = tan1[a];
			
            Vector3 tmp = (t - n * Vector3.Dot(n, t)).normalized;
            tangents[a] = new Vector4(tmp.x, tmp.y, tmp.z);
			
            tangents[a].w = (Vector3.Dot(Vector3.Cross(n, t), tan2[a]) < 0.0f) ? -1.0f : 1.0f;
        }
		
        mesh.tangents = tangents;
    }
}
*/
