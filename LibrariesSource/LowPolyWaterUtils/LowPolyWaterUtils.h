#ifdef TESTFUNCDLL_EXPORT
#define TESTFUNCDLL_API __declspec(dllexport) 
#else
#define TESTFUNCDLL_API __declspec(dllimport) 
#endif

extern "C" {
	int *vertexPair;
	float *vertexPairCacheX;
	float *vertexPairCacheY;
	float *vertexPairCacheZ;

	void PairCheck(int reach, float posX, float posY, float posZ);
	TESTFUNCDLL_API void CreateVertexBuffer(int size, float x, float y, float z);
	TESTFUNCDLL_API int GetPairCheck(int index);
	TESTFUNCDLL_API float GetPairCheckCacheX(int index);
	TESTFUNCDLL_API float GetPairCheckCacheY(int index);
	TESTFUNCDLL_API float GetPairCheckCacheZ(int index);
}