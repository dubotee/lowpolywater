// TestCPPLibrary.cpp : Defines the exported functions for the DLL application.
//

#include "LowPolyWaterUtils.h"

extern "C" {
	extern int *vertexPair;
	extern float *vertexPairCacheX;
	extern float *vertexPairCacheY;
	extern float *vertexPairCacheZ;

	void PairCheck(int reach, float posX, float posY, float posZ)
	{
		for (int i = 0; i < reach; i++)
		{
			if (vertexPairCacheX[i] == posX && vertexPairCacheY[i] == posY && vertexPairCacheZ[i] == posZ)
			{
				vertexPair[reach] = i;
				return;
			}
		}
		vertexPair[reach] = -1;
	}

	void CreateVertexBuffer(int size, float x, float y, float z)
	{
		vertexPair = new int[size * size * 6];
		vertexPairCacheX = new float[size * size * 6];
		vertexPairCacheY = new float[size * size * 6];
		vertexPairCacheZ = new float[size * size * 6];

		int count = 0;

		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				vertexPairCacheX[count] = x + i;
				vertexPairCacheX[count + 1] = x + i + 1;
				vertexPairCacheX[count + 2] = x + i;
				vertexPairCacheX[count + 3] = x + i + 1;
				vertexPairCacheX[count + 4] = x + i;
				vertexPairCacheX[count + 5] = x + i + 1;

				vertexPairCacheY[count] = y;
				vertexPairCacheY[count + 1] = y;
				vertexPairCacheY[count + 2] = y;
				vertexPairCacheY[count + 3] = y;
				vertexPairCacheY[count + 4] = y;
				vertexPairCacheY[count + 5] = y;

				vertexPairCacheZ[count] = z + j;
				vertexPairCacheZ[count + 1] = z + j;
				vertexPairCacheZ[count + 2] = z + j - 1;
				vertexPairCacheZ[count + 3] = z + j;
				vertexPairCacheZ[count + 4] = z + j - 1;
				vertexPairCacheZ[count + 5] = z + j - 1;

				PairCheck(count, x + i, y, z + j);
				PairCheck(count + 1, x + i + 1, y, z + j);
				PairCheck(count + 2, x + i, y, z + j - 1);
				PairCheck(count + 3, x + i + 1, y, z + j);
				PairCheck(count + 4, x + i, y, z + j - 1);
				PairCheck(count + 5, x + i + 1, y, z + j - 1);

				count += 6;
			}
		}
	}

	int GetPairCheck(int index)
	{
		return vertexPair[index];
	}

	float GetPairCheckCacheX(int index)
	{
		return vertexPairCacheX[index];
	}

	float GetPairCheckCacheY(int index)
	{
		return vertexPairCacheY[index];
	}

	float GetPairCheckCacheZ(int index)
	{
		return vertexPairCacheZ[index];
	}
}